<div class="navbar navbar-default" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<div class="collapse navbar-collapse" id="navbar">
			<ul class="nav navbar-nav">
				<li><a href="index.php">Home</a></li>
				<li><a href="index.php?controller=pages&action=newsfeed">News</a></li>
				<li><a href="#">Blog</a></li>
				<li><a href="#">Calendar</a></li>
				<li><a href="#">Library</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<?php if (!$UserManager->login_check()): ?>
				<li><a href="#" data-toggle="modal" data-target="#login_modal">Login</a></li>
				<?php else: ?>
					<?php if($UserManager->admin_check($_SESSION['user_status'])): ?>
						<li><a href="index.php?controller=pages&action=reg">Add user</a></li>
					<?php endif; ?>
				<li><a href="index.php?controller=usermanager&action=logout_na">Logout</a></li>
				<?php endif; ?>
			</ul>
		</div>
	</div>
</div>
<?php
	require_once('./views/login_modal.php');
?>
<style>
	.navbar-default {
		background: white;
		border: 1px solid #e1e8ed;
	}
</style>