<div class="container">
	<div class="row">
		<div class="col-md-8">
			<?php require_once('views/pages/home/followingnews.php'); ?>
		</div>
		<div class="col-md-4">
			<?php require_once('views/pages/home/incomingevents.php'); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?php require_once('views/pages/home/hotnews.php'); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<?php require_once('views/pages/home/library.php'); ?>
		</div>
		<div class="col-md-8">
			<?php require_once('views/pages/home/blog.php'); ?>
		</div>
	</div>
</div>