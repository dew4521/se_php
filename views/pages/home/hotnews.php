<div id="HotNews" class="container-fluid">
	<h4><strong>ข่าวประชาสัมพันธ์</strong></h4>
	<ul class="list">
		<li>
			<div class="hotnews"></div>
		</li>
	</ul>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$.ajax({
			type: "GET",
			url: "http://chromauniapp.ddns.net:3000/news",
			dataType: 'json',
			success: function(result){
				$.each(result, function(index, val){
					$('.hotnews').append('<p>' + val.title + '</p>');
				});
			}
		});
	});
</script>

<style>
	#HotNews {
		background-color: white;
		border: 1px solid #e1e8ed;
		border-radius: 5px;
	}
</style>