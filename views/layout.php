<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
	<?php $UserManager = new UserManager();	?>
	<?php require_once('views/navbar.php'); ?>
	<?php require_once('./routes.php'); ?>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>