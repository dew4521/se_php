<?php
	class PagesController
	{
		public function home() {
			require_once('views/pages/home.php');
		}

		public function newsfeed() {
			require_once('views/pages/newsfeed.php');
		}

		public function profile() {
			require_once('views/pages/profile.php');
		}

		public function reg() {
			require_once('views/register.php');
		}

		public function login_success() {
			require_once('views/pages/protected_page.php');
		}

		public function reg_success() {
			require_once('views/pages/register_success.php');
		}

		public function error() {
			require_once('views/pages/error.php');
		}
	}
?>